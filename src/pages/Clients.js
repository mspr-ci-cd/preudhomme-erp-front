import React, { useState, useEffect } from "react";
import env from "react-dotenv";
import axios from "axios";

/**
 * Client Component
 *
 * @version 1.0.0
 * @author [Jules Peguet](https://github.com/j-peguet)
 */
export default function Clients() {
    useEffect(() => {
        // Met à jour le titre du document via l’API du navigateur
        getClients();
    }, []);

    const [clients, setClients] = useState([]);
   
    const getClients = () => {
        axios.get(`${env.API_URL_CLIENT}/client`)
            .then(result => { 
                setClients(result.data);
            });
    };

    return (
        <div className="container">
            <h1>Clients List</h1>
            <div>
                <table>
                    <thead>
                    <tr>
                        <th>FirstName</th>
                        <th>LastName</th>
                        <th>Phone Number</th>
                        <th>Accounts</th>
                    </tr>
                    </thead>
                    <tbody>
                        {
                            clients.map(client => {
                                return(
                                    <tr key={client.id}>
                                        <td>{client.first_name}</td>
                                        <td>{client.last_name}</td>
                                        <td>{client.phone_number}</td>
                                        <td>
                                            {
                                                client.account.map(account => {
                                                    return(
                                                        <div>
                                                            {account.type} - ${account.balance}
                                                            <hr/>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>
    );
};