Exemple d'utilisation du component Clients:

La configuration d'une variable d'environment `API_URL_CLIENT` est nécessaire

```jsx static
import Clients from './Clients'
<div>
    <Clients/>
</div>
```