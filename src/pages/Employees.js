import React, { useState, useEffect } from "react";
import env from "react-dotenv";
import axios from "axios";

/**
 * Employee Component
 *
 * @version 1.0.0
 * @author [Jules Peguet](https://github.com/j-peguet)
 */
export default function Employees() {
    useEffect(() => {
        // Met à jour le titre du document via l’API du navigateur
        getEmployees();
    }, []);
    
    const [employees, setEmployees] = useState([]);
   
    const getEmployees = () => {
        axios.get(`${env.API_URL_EMPLOYEE}/employee`)
            .then(result => { 
                setEmployees(result.data);
        });
    };

    return (
        <div className="container">
            <h1>Employees list</h1>
            <div>
                <table>
                    <thead>
                    <tr>
                        <th>FirstName</th>
                        <th>LastName</th>
                        <th>Phone Number</th>
                        <th>Service</th>
                    </tr>
                    </thead>
                    <tbody>
                        {
                            employees.map(employee => {
                                return(
                                    <tr key={employee.id}>
                                        <td>{employee.first_name}</td>
                                        <td>{employee.last_name}</td>
                                        <td>{employee.phone_number}</td>
                                        <td>{employee.service}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>
    );
}