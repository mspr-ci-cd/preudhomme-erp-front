Exemple d'utilisation du component Employees:

La configuration d'une variable d'environment `API_URL_EMPLOYEE` est nécessaire

```jsx static
import Employees from './Employees'
<div>
    <Employees/>
</div>
```