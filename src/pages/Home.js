import React from "react";

/**
 * Home Component
 *
 * @version 1.0.0
 * @author [Jules Peguet](https://github.com/j-peguet)
 */
export default function Home() {
    return (
        <div className="container">
            <h1>Bank V2</h1>
        </div>
    );
};
