import { render, screen } from '@testing-library/react';
import Home from '../pages/Home';

test('renders Bank Home Menu', () => {
  render(<Home />);
  const linkElement = screen.getByText(/Bank/i);
  expect(linkElement).toBeInTheDocument();
});
